﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : MonoBehaviour
{
    private float speed = 5f;
    private float Horizontal;
    private float Vertical;

    private Vector2 movement;

    public Rigidbody2D rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxis("Horizontal");
        Vertical = Input.GetAxis("Vertical");

        movement = new Vector2(Horizontal, Vertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider collide)
    {
        if(collide.gameObject.CompareTag("Pick Up"))
        {
            collide.gameObject.SetActive(false);
        }
    }
}
